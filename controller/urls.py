from django.conf.urls import url
from controller.views import create_contig

urlpatterns = [
    url(r'^create/$', create_contig),
]