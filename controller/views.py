from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import renderers
from rest_framework import viewsets, generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import TemplateHTMLRenderer, DocumentationRenderer
from .serializers import *
from .models import *
from django.views.decorators.csrf import csrf_exempt


# @csrf_exempt
# class CreateConfig(generics.CreateAPIView):
#     queryset = Project.objects.all()
#     serializer_class = JobSerializer
    # renderer_classes = (TemplateHTMLRenderer,)
    #
    # def post(self, request, *args, **kwargs):
    #     project = Project.objects.filter(job__id=request.data['job']['id'])
    #     return Response({'project':project}, template_name='edge_config.tmpl')

@csrf_exempt
@api_view(['GET', 'POST'])
@renderer_classes((TemplateHTMLRenderer,))
def create_contig(request):
    project = Project.objects.get(job__id=request.data['job']['id'])
    user_project = UserProjects.objects.get(project_id=project.id)
    quality_trim, stitch_paired_ends, host_removal = None, None, None
    sra_input = []
    if SRAInput.objects.filter(job__id=request.data['job']['id']).exists():
        sra_input = SRAInput.objects.filter(job__id=request.data['job']['id'])
    # read_fastq_input = ReadFastqInput.objects.get(job__id=request.data['job']['id'])
    if QualityTrimFilter.objects.filter(job__id=request.data['job']['id']).exists():
        quality_trim = QualityTrimFilter.objects.get(job__id=request.data['job']['id'])
    if StitchPairedEnds.objects.filter(job__id=request.data['job']['id']).exists():
        stitch_paired_ends = StitchPairedEnds.objects.get(job__id=request.data['job']['id'])
    if HostRemoval.objects.filter(job__id=request.data['job']['id']).exists():
        host_removal = HostRemoval.objects.get(job__id=request.data['job']['id'])

    proj_serializer = ProjectSerializer(project)
    user_proj_serializer= UserProjectsSerializer(user_project)
    template_dict = {'user_project':user_proj_serializer.data, 'project':proj_serializer.data}
    if len(sra_input)>0:
        sra_serializer = SRAInputSerializer(sra_input[0])
        template_dict.update(sra_serializer.data)
    # read_serializer = ReadFastQInputSerializer(read_fastq_input)
    if quality_trim:
        quality_serializer = QualityTrimFilterSerializer(quality_trim)
        template_dict.update(quality_serializer.data)
    if stitch_paired_ends:
        stitch_serializer = StitchPairedEndsSerializer(stitch_paired_ends)
        template_dict.update(stitch_serializer.data)
    if host_removal:
        host_serializer = HostRemovalSerializer(host_removal)
        template_dict.update(host_serializer.data)

    return Response(template_dict, template_name='controller/edge_config.tmpl')

class UserViewSet(viewsets.ModelViewSet):

    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):

    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class MetadataViewSet(viewsets.ModelViewSet):
    queryset = Metadata.objects.all()
    serializer_class = MetadataSerializer


class JobViewSet(viewsets.ModelViewSet):
    queryset = Job.objects.all()
    serializer_class = JobSerializer


class ContigFastaInputViewSet(viewsets.ModelViewSet):
    queryset = ContigFastaInput.objects.all()
    serializer_class = ContigFastaInputSerializer


class ReadFastQInputViewSet(viewsets.ModelViewSet):
    queryset = ReadFastqInput.objects.all()
    serializer_class = ReadFastQInputSerializer


class SRAInputViewSet(viewsets.ModelViewSet):
    queryset = SRAInput.objects.all()
    serializer_class = SRAInputSerializer


class QualityTrimFilterViewSet(viewsets.ModelViewSet):
    queryset = QualityTrimFilter.objects.all()
    serializer_class = QualityTrimFilterSerializer


class StitchPairedEndsViewSet(viewsets.ModelViewSet):
    queryset = StitchPairedEnds.objects.all()
    serializer_class = StitchPairedEndsSerializer


class HostGenomeViewSet(viewsets.ModelViewSet):
    queryset = HostGenome.objects.all()
    serializer_class = HostGenomeSerializer


class HostRemovalViewSet(viewsets.ModelViewSet):
    queryset = HostRemoval.objects.all()
    serializer_class = HostRemovalSerializer


class AssemblyViewSet(viewsets.ModelViewSet):
    queryset = Assembly.objects.all()
    serializer_class = AssemblySerializer


class AnnotationViewSet(viewsets.ModelViewSet):
    queryset = Annotation.objects.all()
    serializer_class = AnnotationSerializer


class ReferenceGenomeViewSet(viewsets.ModelViewSet):
    queryset = ReferenceGenome.objects.all()
    serializer_class = ReferenceGenomeSerializer


class ReferenceBasedAnalysisViewSet(viewsets.ModelViewSet):
    queryset = ReferenceBasedAnalysis.objects.all()
    serializer_class = ReferenceBasedAnalysisSerializer


class TaxonomyClassificationViewSet(viewsets.ModelViewSet):
    queryset = TaxonomyClassification.objects.all()
    serializer_class = TaxonomyClassificationSerializer


class PhylogeneticAnalysisViewSet(viewsets.ModelViewSet):
    queryset = PhylogeneticAnalysis.objects.all()
    serializer_class = PhylogeneticAnalysisSerializer


class GeneFamilyAnalysisViewSet(viewsets.ModelViewSet):
    queryset = GeneFamilyAnalysis.objects.all()
    serializer_class = GeneFamilyAnalysisSerializer


class PrimerValidationViewSet(viewsets.ModelViewSet):
    queryset = PrimerValidation.objects.all()
    serializer_class = PrimerValidationSerializer


class PrimerDesignViewSet(viewsets.ModelViewSet):
    queryset = PrimerDesign.objects.all()
    serializer_class = PrimerDesignSerializer