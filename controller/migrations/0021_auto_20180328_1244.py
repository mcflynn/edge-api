# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-28 12:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('controller', '0020_auto_20180327_2320'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assembly',
            name='nanopore_reads_fastq',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='assembly',
            name='pacbio_subreads_fastq',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='hostremoval',
            name='host_file',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='phylogeneticanalysis',
            name='reference_genome_file',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='primervalidation',
            name='primer_fasta_sequences',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='readfastqinput',
            name='job',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='fastq_read_file', to='controller.Job'),
        ),
        migrations.AlterField(
            model_name='referencebasedanalysis',
            name='max_coverage',
            field=models.IntegerField(default=300),
        ),
        migrations.AlterField(
            model_name='referencebasedanalysis',
            name='min_baseq',
            field=models.IntegerField(default=20),
        ),
        migrations.AlterField(
            model_name='referencebasedanalysis',
            name='reference_genome_file',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='taxonomyclassification',
            name='custom_db_tool',
            field=models.CharField(choices=[('GOTTCHA_BACTERIAL_GENUS', 'gottcha_bacterial_genus'), ('GOTTCHA_BACTERIAL_SPECIES', 'gottcha_bacterial_species'), ('GOTTCHA_BACTERIAL_STRAIN', 'gottcha_bacterial_strain'), ('GOTTCHA_VIRAL_GENUS', 'gottcha_viral_genus'), ('GOTTCHA_VIRAL_SPECIES', 'gottcha_viral_species'), ('GOTTCHA_VIRAL_STRAIN', 'gottcha_viral_strain'), ('READS_MAPPING', 'reads_mapping'), ('METAPHLAN', 'metaphlan'), ('KRAKEN', 'kraken')], max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='taxonomyclassification',
            name='gottcha_spedb_v_file',
            field=models.FileField(null=True, upload_to=''),
        ),
    ]
