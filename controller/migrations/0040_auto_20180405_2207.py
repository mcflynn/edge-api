# Generated by Django 2.0.2 on 2018-04-05 22:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controller', '0039_project_num_cpus'),
    ]

    operations = [
        migrations.RenameField(
            model_name='project',
            old_name='code',
            new_name='edge_proj_code',
        ),
        migrations.RenameField(
            model_name='project',
            old_name='num_cpus',
            new_name='edge_proj_cpu',
        ),
        migrations.RenameField(
            model_name='project',
            old_name='description',
            new_name='edge_proj_desc',
        ),
        migrations.RenameField(
            model_name='project',
            old_name='name',
            new_name='edge_proj_name',
        ),
        migrations.RenameField(
            model_name='project',
            old_name='runhost',
            new_name='edge_proj_runhost',
        ),
        migrations.RenameField(
            model_name='project',
            old_name='status',
            new_name='edge_proj_status',
        ),
    ]
