# Generated by Django 2.0.2 on 2018-03-27 19:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controller', '0016_annotation_job'),
    ]

    operations = [
        migrations.RenameField(
            model_name='readfastqfile',
            old_name='file_name',
            new_name='fastq_read_file',
        ),
    ]
