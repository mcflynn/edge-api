# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-04-04 00:20
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controller', '0037_auto_20180403_2019'),
    ]

    operations = [
        migrations.RenameField(
            model_name='phylogeneticanalysis',
            old_name='boostrap_number',
            new_name='bootstrap_number',
        ),
    ]
