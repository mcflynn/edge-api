# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-28 23:19
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('controller', '0024_auto_20180328_2239'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hostgenome',
            name='job',
        ),
    ]
