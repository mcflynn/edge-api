# Generated by Django 2.0.2 on 2018-03-22 21:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('controller', '0005_auto_20180322_1952'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='sra',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='controller.SRAInput'),
        ),
    ]
