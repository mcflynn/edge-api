from django.db import models
from django.contrib.auth.models import User, Group
from django.utils import timezone
from multiselectfield import MultiSelectField
BOWTIE2 = 'BOWTIE2'
BWAMEM = 'BWAMEM'
MINIMAP2 = 'MINIMAP2'
ALIGNER_CHOICES = ((BOWTIE2, 'Bowtie2'), (BWAMEM, 'BWA_mem'), (MINIMAP2, 'Minimap2'))
import os

class Project(models.Model):
    EP = 'EDGE_PROD'
    ED = 'EDGE_DEV'
    HOST_CHOICES = ((EP, 'edge-prod'), (ED, 'edge_dev'))
    YES = 'YES'
    NO = 'NO'
    PUBLISHED_CHOICES = ((YES, 'yes'), (NO, 'no'))
    edge_proj_name = models.CharField(max_length=255, unique=True)
    published = models.CharField(max_length=25, default='no', choices=PUBLISHED_CHOICES)
    edge_proj_desc = models.TextField(null=True)
    created = models.DateTimeField(auto_created=True, default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)
    edge_proj_status = models.CharField(max_length=255, default='in_process')
    edge_proj_code = models.CharField(max_length=30, unique=True, null=True)
    edge_proj_runhost = models.CharField(max_length=12, choices=HOST_CHOICES, default=EP)
    edge_proj_cpu = models.IntegerField(default=4)

    def __str__(self):
        return self.edge_proj_name


def user_directory_path(instance):
    return 'user_{0}'.format(instance.owner.id)


def generate_user_storage_paths(instance):
    instance.path = user_directory_path(instance)
    instance.save()
    if not os.path.exists(instance.path):
        os.makedirs(instance.path)


class UserProjects(models.Model):
    OWNER = 'OWNER'
    GUEST = 'GUEST'
    ROLE_CHOICES = ((OWNER, 'owner'), (GUEST, 'guest'))
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_path = models.FilePathField(path=generate_user_storage_paths)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    role = models.CharField(max_length=25, default=OWNER, choices=ROLE_CHOICES)


class Metadata(models.Model):
    ANIMAL = 'ANIMAL'
    ENV = 'ENVIRONMENTAL'
    HUMAN = 'HUMAN'
    SAMPLE_CHOICES = ((ANIMAL,'animal'),(ENV, 'environmental'), (HUMAN, 'human'))
    HEALTHY = 'HEALTHY'
    DISEASED = 'DISEASED'
    UNKNOWN = 'UNKNOWN'
    MALE='male'
    FEMALE='female'
    HOST_CONDITION_CHOICES = ((HEALTHY, 'healthy'), (DISEASED, 'diseased'), (UNKNOWN, 'unknown'))
    HOST_GENDER_CHOICES = ((MALE, 'MALE'), (FEMALE, 'FEMALE'))
    CARDIOVASCULAR='Cardiovascular'
    CONSTITUTIONAL='Constitutional'
    ENT_MOUTH='ENT\Mouth'
    EYES = 'Eyes'
    GASTROINTESTINAL = 'Gastrointestinal'
    HEME_LYMPH = 'Heme\Lymph'
    MUSCULOSKELETAL = 'Musculoskeletal'
    NEUROLOGIC = 'Neurologic'
    RESPIRATORY = 'Respiratory'
    SKIN = 'Skin'
    URINARY='Urinary'
    SYMPTOM_CHOICES = ((CARDIOVASCULAR,('1', 'Cardiovascular')))
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    metadata_study_title = models.CharField(max_length=30)
    metadata_study_type = models.CharField(max_length=30)
    metadata_sample_name = models.CharField(max_length=240)
    metadata_sample_type = models.CharField(max_length=12, choices=SAMPLE_CHOICES)
    metadata_host = models.CharField(max_length=30)
    metadata_host_condition = models.CharField(max_length=12,choices=HOST_CONDITION_CHOICES)
    metadata_host_gender = models.CharField(max_length=12, choices=HOST_GENDER_CHOICES)
    metadata_host_age = models.IntegerField()
    collection_date = models.DateField()
    address = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45, blank=True, null=True)
    state = models.CharField(max_length=30, blank=True, null=True)
    country = models.CharField(max_length=30, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    experiment_title = models.CharField(max_length=45, blank=True, null=True)
    sequencing_center = models.CharField(max_length=45, blank=True, null=True)
    sequencing_date = models.DateField(blank=True, null=True)


class Job(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    start_time = models.DateTimeField(auto_created=True, default=timezone.now)

    def __str__(self):
        return "{0}-{1}".format(str(self.project_id),self.start_time)


class Tool(models.Model):
    do_run = models.BooleanField(default=False)

    class Meta:
        abstract = True


class SRAInput(models.Model):
    job = models.ForeignKey(Job, related_name='sra', on_delete=models.CASCADE)
    edge_sra_sw = models.BooleanField(default=False)
    edge_sra_acc = models.CharField(max_length=30)

    class Meta(Tool.Meta):
        verbose_name = 'Get SRA input'

    def __str__(self):
        return '{0}-{1}'.format(self.job_id, self.edge_sra_acc)


class ReadFastqInput(Tool):
    job = models.ForeignKey(Job, related_name='read_fastq_input', on_delete=models.CASCADE)
    fastq_read_file = models.FilePathField()

    PAIR1 = 'pair1_read_file'
    PAIR2 = 'pair2_read_file'
    SINGLE = 'single_end_read_file'
    FILE_TYPE_CHOICES = ((PAIR1, 'pe1'), (PAIR2, 'pe2'), (SINGLE, 'se'))

    file_type = models.CharField(max_length=3, choices=FILE_TYPE_CHOICES)


class ContigFastaInput(models.Model):
    job = models.ForeignKey(Job, related_name='contig_fasta_input', on_delete=models.CASCADE)
    edge_input_contig_file = models.FilePathField()

    class Meta(Tool.Meta):
        verbose_name = 'Read FASTA input file'

    def __str__(self):
        return self.edge_input_contig_file.split('/')[-1]


class PreProcessing(Tool):
    class Meta:
        abstract = True


class QualityTrimFilter(models.Model):
    job = models.ForeignKey(Job, related_name='quality_trim_filter', on_delete=models.CASCADE)
    edge_qc_sw = models.BooleanField(default=False)
    edge_qc_q = models.IntegerField(default=5)
    edge_qc_minl = models.IntegerField(default=50)
    edge_qc_avgq = models.IntegerField(default=0)
    edge_qc_n = models.IntegerField(default=10)
    edge_qc_lc = models.FloatField(default=0.85)
    edge_qc_adapter = models.FilePathField(blank=True, null=True)
    edge_qc_polyA = models.BooleanField(default=False)
    edge_qc_5end = models.IntegerField(default=0)
    edge_qc_3end = models.IntegerField(default=0)

    class Meta(Tool.Meta):
        verbose_name = 'Read FASTA input file'

    def __str__(self):
        return "QTrimFilter-{0}-{1}-{2}-{3}".format(str(self.trim_quality_level), str(self.min_read_length),
                                                    str(self.avg_quality_cutoff), str(self.n_base_cutoff))


class StitchPairedEnds(models.Model):
    job = models.ForeignKey(Job, related_name='stitch_paired_ends', on_delete=models.CASCADE)
    edge_joinpe_sw = models.BooleanField(default=False)
    edge_joinpe_maxdiff = models.IntegerField(default=8)
    edge_joinpe_minoverlap = models.IntegerField(default=6)
    edge_joinpe_usejoined_only = models.BooleanField(default=False)

    def __str__(self):
        return "Stitched-{0}-{1}-{2}".format(str(self.n_percent_max_difference), str(self.n_minimum_overlap),
                                             str(self.use_joined_paired_ends_only))


class HostGenome(models.Model):
    name = models.CharField(max_length=45)
    path = models.CharField(max_length=255)

    def __str__(self):
        return 'host-{0}'.format(self.name)


class HostRemoval(models.Model):
    job = models.OneToOneField(Job, related_name='host_removal', on_delete=models.CASCADE)
    edge_hostrm_sw = models.BooleanField(default=False)
    host_genomes = models.ManyToManyField(HostGenome, related_name='host_genomes')
    edge_hostrm_file = models.FilePathField(null=True, blank=True)
    edge_hostrm_similarity = models.IntegerField(default=90)

    def __str__(self):
        return str(self.host_file)


class AssemblyAnnotation(Tool):
    class Meta:
        abstract = True


class Assembly(models.Model):
    IBDA_UD = 'IBDA_UD'
    SPADES = 'SPAdes'
    MEGAHIT = 'MEGAHIT'
    ASSEMBLER_CHOICES = ((IBDA_UD, 'IDBA_UD'), (SPADES, 'SPAdes'), (MEGAHIT, 'MEGAHIT'))

    job = models.ForeignKey(Job, related_name='assembly', on_delete=models.CASCADE)
    edge_assembly_sw = models.BooleanField(default=False)
    edge_assembler = models.CharField(max_length=10, choices=ASSEMBLER_CHOICES, default=IBDA_UD)
    edge_assembly_mink = models.IntegerField(default=31)
    edge_assembly_maxk = models.IntegerField(default=121)
    edge_assembly_step = models.IntegerField(default=20)
    edge_assembly_minc= models.IntegerField(default=200)
    edge_r2c_aligner = models.CharField(max_length=10, choices=ALIGNER_CHOICES, default=BOWTIE2)
    DEFAULT = 'DEFAULT'
    SINGLE_CELL = 'SINGLE_CELL'
    METAGENOME = 'METAGENOME'
    PLASMIDS = 'PLASMIDS'
    RNA = 'RNA'
    SPADES_ALGORITHM_CHOICES = ((DEFAULT, 'default'), (SINGLE_CELL, 'single_cell'), (METAGENOME, 'metagenome'),
                                (PLASMIDS, 'plasmids'), (RNA, 'rna'))
    edge_spades_algorithm = models.CharField(max_length=10, choices=SPADES_ALGORITHM_CHOICES, default=DEFAULT)
    edge_spades_pacbio_file = models.FilePathField(null=True)
    edge_spades_nanopore_file = models.FilePathField(null=True)
    edge_assembled_contig_sw = models.BooleanField(default=False)
    edge_assembled_contig_file = models.FilePathField()

    META = 'META'
    META_SENSITIVE = 'META_SENSITIVE'
    META_LARGE = 'META_LARGE'
    BULK = 'BULK'
    PRESET_CHOICES = ((META, 'meta'), (META_SENSITIVE, 'meta_sensitive'), (META_LARGE, 'meta_large'), (BULK, 'bulk'),
                      (SINGLE_CELL, 'single_cell'))
    edge_megahit_preset = models.CharField(max_length=12, choices=PRESET_CHOICES, default=META)
    edge_r2c_aligner_options = models.CharField(max_length=100, null=True)
    edge_r2c_getunmapped_sw = models.BooleanField(default=False)


class Annotation(models.Model):
    job = models.ForeignKey(Job, related_name='annotation', on_delete=models.CASCADE)
    edge_anno_sw = models.BooleanField(default=False)
    edge_anno_size_cut =  models.IntegerField(default=700)

    PROKKA = 'PROKKA'
    RATT = 'RATT'
    ANNOTATION_TOOL_CHOICES = ((PROKKA, 'prokka'), (RATT, 'ratt'))
    edge_anno_tool = models.CharField(max_length=10, choices=ANNOTATION_TOOL_CHOICES, default=PROKKA)

    ARCHAEA = 'ARCHAEA'
    BACTERIA = 'BACTERIA'
    MITOCHONDRIA = 'MITOCHONDRIA'
    VIRUSES = 'VIRUSES'
    METAGENOME = 'METAGENOME'
    KINGDOM_CHOICES = ((ARCHAEA, 'archaea'),(BACTERIA, 'bacteria'), (MITOCHONDRIA, 'mitochondria'),
                       (VIRUSES, 'viruses'), (METAGENOME, 'metagenome'))
    edge_anno_kingdom = models.CharField(max_length=20, default=BACTERIA)
    edge_anno_source_file = models.FilePathField()


class ReferenceGenome(models.Model):
    name = models.CharField(max_length=100, unique=True)
    locus = MultiSelectField(max_length=20)

    def __unicode__(self):
        return self.name


class ReferenceGenomeFile(models.Model):
    name = models.FilePathField()


class ReferenceBasedAnalysis(models.Model):

    job = models.OneToOneField(Job, related_name='reference_based_analysis', on_delete=models.CASCADE)
    edge_ref_sw = models.BooleanField(default=False)
    edge_ref_file_fromlist = models.ManyToManyField(ReferenceGenome, related_name='reference_genomes')
    edge_ref_file = models.ManyToManyField(ReferenceGenomeFile, related_name='reference_genome_files', blank=True)
    edge_r2g_aligner = models.CharField(max_length=20, choices=ALIGNER_CHOICES, default=BOWTIE2)
    edge_r2g_variantcall_sw = models.BooleanField(default=True)
    edge_ref_unmapped_read_class = models.BooleanField(default=False)
    edge_r2g_getconsensus_sw = models.BooleanField(default=False)
    edge_r2g_aligner_options = models.CharField(max_length=100, null=True)
    edge_r2g_getmapped_sw = models.BooleanField(default=False)
    edge_r2g_getunmapped_sw = models.BooleanField(default=False)
    edge_r2g_con_min_mapQ = models.IntegerField(default=10)
    edge_r2g_con_min_cov = models.IntegerField(default=5)
    edge_r2g_con_max_cov = models.IntegerField(default=300)
    edge_r2g_con_min_baseQ = models.IntegerField(default=20)
    edge_r2g_con_alt_prop = models.FloatField(default=0.5)


class TaxonomyClassification(models.Model):
    job = models.OneToOneField(Job, related_name='taxonomy_classification', on_delete=models.CASCADE)
    edge_taxa_sw = models.BooleanField(default=True)
    GOTTCHA_BACTERIAL_GENUS = 'GOTTCHA_BACTERIAL_GENUS'
    GOTTCHA_BACTERIAL_SPECIES = 'GOTTCHA_BACTERIAL_SPECIES'
    GOTTCHA_BACTERIAL_STRAIN = 'GOTTCHA_BACTERIAL_STRAIN'
    GOTTCHA_VIRAL_GENUS = 'GOTTCHA_VIRAL_GENUS'
    GOTTCHA_VIRAL_SPECIES = 'GOTTCHA_VIRAL_SPECIES'
    GOTTCHA_VIRAL_STRAIN = 'GOTTCHA_VIRAL_STRAIN'
    GOTTCHA2_BACTERIAL_SPECIES = 'GOTTCHA2_BACTERIAL_SPECIES'
    GOTTCHA2_VIRAL_SPECIES = 'GOTTCHA2_VIRAL_SPECIES'
    PANGIA_NCBI_GENOMES_P_GRCH38 = 'PANGIA_NCBI_GENOMES_P_GRCH38'
    READS_MAPPING = 'READS_MAPPING'
    METAPHLAN = 'METAPHLAN'
    KRAKEN = 'KRAKEN'
    DIAMOND = 'DIAMOND'
    CLASSIFICATION_TOOL_CHOICES = (
        (GOTTCHA_BACTERIAL_GENUS, 'gottcha_bacterial_genus'),
        (GOTTCHA_BACTERIAL_SPECIES, 'gottcha_bacterial_species'),
        (GOTTCHA_BACTERIAL_STRAIN, 'gottcha_bacterial_strain'),
        (GOTTCHA_VIRAL_GENUS, 'gottcha_viral_genus'),
        (GOTTCHA_VIRAL_SPECIES, 'gottcha_viral_species'),
        (GOTTCHA_VIRAL_STRAIN, 'gottcha_viral_strain'),
        (GOTTCHA2_BACTERIAL_SPECIES, 'gottcha2_bacterial_species'),
        (GOTTCHA2_VIRAL_SPECIES, 'gottcha2_viral_species'),
        (PANGIA_NCBI_GENOMES_P_GRCH38, 'pangia_ncbi_genomes_p_grch38'),
        (READS_MAPPING, 'reads_mapping'),
        (METAPHLAN, 'metaphlan'),
        (KRAKEN, 'kraken'),
        (DIAMOND, 'diamond'),
    )
    edge_taxa_enabled_tools = MultiSelectField(max_length=30, choices=CLASSIFICATION_TOOL_CHOICES)
    edge_taxa_allreads = models.BooleanField(default=True)

    splitrim_minq = models.IntegerField(default=20)
    CUSTOM_DB_TOOL_CHOICES = ((GOTTCHA_BACTERIAL_GENUS, 'gottcha_bacterial_genus'),
                              (GOTTCHA_BACTERIAL_SPECIES, 'gottcha_bacterial_species'),
                              (GOTTCHA_BACTERIAL_STRAIN, 'gottcha_bacterial_strain'),
                              (GOTTCHA_VIRAL_GENUS, 'gottcha_viral_genus'),
                              (GOTTCHA_VIRAL_SPECIES, 'gottcha_viral_species'),
                              (GOTTCHA_VIRAL_STRAIN, 'gottcha_viral_strain'),
                              (READS_MAPPING, 'reads_mapping'),
                              (METAPHLAN, 'metaphlan'),
                              (KRAKEN, 'kraken')
                              )

    edge_taxa_custom_db_tool = models.CharField(max_length=20, choices=CUSTOM_DB_TOOL_CHOICES, null=True)
    edge_taxa_custom_db_file = models.FilePathField(null=True)
    edge_contig_taxa_sw = models.BooleanField(default=True)


class SRAAccessions(models.Model):
    number = models.CharField(max_length=12)


class PhyloReferenceGenomeFile(models.Model):
    edge_phylo_sra_acc = models.FilePathField()


class PhylogeneticAnalysis(Tool):
    job = models.OneToOneField(Job, related_name='phylogenetic_analysis', on_delete=models.CASCADE)
    edge_phylo_sw = models.BooleanField(default=False)
    FASTTREE = 'FASTTREE'
    RAXML = 'RAXML'
    TREE_BUILD_METHOD_CHOICES = ((FASTTREE, 'fasttree'), (RAXML, 'raxml'))
    edge_phylo_tree = models.CharField(max_length=10, choices= TREE_BUILD_METHOD_CHOICES, default=FASTTREE)

    ECOLI = 'ECOLI'
    YERSINIA = 'YERSINIA'
    FRANCISELLA = 'FRANCISELLA'
    BRUCELLA = 'BRUCELLA'
    BACILLUS = 'BACILLUS'
    PATHOGEN_CHOICES = ((ECOLI, 'ecoli'), (YERSINIA, 'yersinia'), (FRANCISELLA, 'francisella'), (BRUCELLA, 'brucella'),
                        (BACILLUS, 'bacillus'))
    edge_phylo_patho = models.CharField(max_length=15, choices=PATHOGEN_CHOICES, null=True)

    edge_phylo_ref_select = models.ManyToManyField(ReferenceGenome, related_name='phylo_reference_genomes')
    edge_phylo_ref_list_file = models.ManyToManyField(PhyloReferenceGenomeFile,
                                                      related_name='phylo_reference_genome_files')
    edge_phylo_ref_select_ref = models.ForeignKey(ReferenceGenome, related_name='phylo_ref_select_ref',
                                                  on_delete=models.CASCADE)
    edge_phylo_sra_acc = models.ManyToManyField(SRAAccessions, related_name='edge_phylo_sra_acc')
    edge_phylo_bootstrap_sw = models.BooleanField(default=False)
    edge_phylo_bootstrap_num = models.IntegerField(default=100)

class GeneFamilyAnalysis(Tool):
    job = models.ForeignKey(Job, related_name='gene_family_analysis', on_delete=models.CASCADE)
    edge_sg_sw = models.BooleanField(default=False)
    edge_reads_sg_sw = models.BooleanField(default=True)
    edge_orfs_sg_sw = models.BooleanField(default=True)
    edge_sg_stool = models.CharField(max_length=20, default='rapsearch2')
    edge_sg_identity_options = models.FloatField(default=0.95)
    edge_sg_length_options = models.FloatField(default=0.95)


class PCRPrimerAnalysis(Tool):
    class Meta:
        abstract = True


class PrimerValidation(PCRPrimerAnalysis):
    job = models.ForeignKey(Job, related_name='primer_validation', on_delete=models.CASCADE)
    edge_primer_sw = models.BooleanField(default=False)
    edge_primer_valid_sw = models.BooleanField(default=False)
    edge_primer_valid_file = models.FilePathField(null=True)
    edge_primer_valid_mm = models.IntegerField(default=1)


class PrimerDesign(PCRPrimerAnalysis):
    job = models.ForeignKey(Job, related_name='primer_design', on_delete=models.CASCADE)
    edge_primer_adj_sw = models.BooleanField(default=False)
    edge_primer_adj_tm_opt = models.IntegerField(default=59)
    edge_primer_adj_tm_min = models.IntegerField(default=57)
    edge_primer_adj_tm_max = models.IntegerField(default=63)
    edge_primer_adj_len_opt = models.IntegerField(default=20)
    edge_primer_adj_len_min = models.IntegerField(default=18)
    edge_primer_adj_len_max = models.IntegerField(default=27)
    edge_primer_adj_df = models.IntegerField(default=5)
    edge_primer_adj_num = models.IntegerField(default=5)
