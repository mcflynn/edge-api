from django.contrib.auth.models import User, Group
from rest_framework import serializers, fields
import bleach
from .models import *


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

class UserProjectsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProjects
        fields = ('user', 'project')
class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('edge_proj_name', 'edge_proj_desc', 'created', 'updated', 'edge_proj_code', 'edge_proj_runhost', 'edge_proj_cpu', 'edge_proj_status')

    # replace dashes sent by edge.js with underscores
    def to_internal_value(self, data):
        return dict(zip([('_').join(y) for y in [x.split('-') for x in data]], data.values()))


class MetadataSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Metadata
        fields = ('project', 'study_name', 'study_type', 'sample_name', 'sample_type', 'host', 'host_condition',
                  'collection_date')
    # replace dashes sent by edge.js with underscores
    def to_internal_value(self, data):
        return dict(zip([('_').join(y) for y in [x.split('-') for x in data]], data.values()))


class ReadFastQInputSerializer(serializers.ModelSerializer):
    fastq_read_file = serializers.FilePathField(recursive=True, path=os.path.join(
        os.path.dirname(os.path.dirname(__file__)), 'user_files'))
    class Meta:
        model = ReadFastqInput
        fields = ('fastq_read_file', 'file_type')

    # replace dashes sent by edge.js with underscores
    def to_internal_value(self, data):
        return dict(zip([('_').join(y) for y in [x.split('-') for x in data]], data.values()))

class AdapterFastaSerializer(serializers.Serializer):
    adapter_fasta = serializers.FilePathField(recursive=True, path=os.path.join(
        os.path.dirname(os.path.dirname(__file__)), 'user_files'))


class ContigFastaInputSerializer(serializers.ModelSerializer):
    fasta_file = serializers.FilePathField(recursive=True, path=os.path.join(
        os.path.dirname(os.path.dirname(__file__)), 'user_files'))
    class Meta:
        model = ContigFastaInput
        fields = ('fasta_file','job')

    # replace dashes sent by edge.js with underscores
    def to_internal_value(self, data):
        return dict(zip([('_').join(y) for y in [x.split('-') for x in data]], data.values()))


class QualityTrimFilterSerializer(serializers.ModelSerializer):
    adapter_fasta = AdapterFastaSerializer(many=True, required=False)

    class Meta:
        model = QualityTrimFilter
        fields = ('edge_qc_sw', 'edge_qc_q', 'edge_qc_avgq', 'edge_qc_minl', 'edge_qc_n', 'edge_qc_lc',
                  'edge_qc_adapter', 'edge_qc_polyA', 'edge_qc_5end', 'edge_qc_3end')

    # replace dashes sent by edge.js with underscores
    def to_internal_value(self, data):
        return dict(zip([('_').join(y) for y in [x.split('-') for x in data]], data.values()))

class StitchPairedEndsSerializer(serializers.ModelSerializer):
    class Meta:
        model = StitchPairedEnds
        fields = ('edge_joinpe_sw', 'edge_joinpe_maxdiff', 'edge_joinpe_minoverlap', 'edge_joinpe_usejoined_only')

    # replace dashes sent by edge.js with underscores
    def to_internal_value(self, data):
        return dict(zip([('_').join(y) for y in [x.split('-') for x in data]], data.values()))

class HostGenomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = HostGenome
        fields = ('name',)


class HostRemovalSerializer(serializers.ModelSerializer):
    edge_hostrm_file_fromlist = serializers.SlugRelatedField(many=True, required=False, queryset=HostGenome.objects.all(), slug_field='name')
    edge_hostrm_file = AdapterFastaSerializer(many=True, required=False)
    # host_file = serializers.FilePathField(recursive=True, path=os.path.join(os.path.dirname(os.path.dirname(__file__)),
    #                                                                         'user_files'))
    class Meta:
        model = HostRemoval
        fields = ('edge_hostrm_sw', 'edge_hostrm_file_fromlist', 'edge_hostrm_file', 'edge_hostrm_similarity')

    # replace dashes sent by edge.js with underscores
    def to_internal_value(self, data):
        return dict(zip([('_').join(y) for y in [x.split('-') for x in data]], data.values()))


class SRAInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = SRAInput
        fields = ('edge_sra_acc',)


class AssemblySerializer(serializers.ModelSerializer):
    class Meta:
        model = Assembly
        fields = ('edge_assembly_sw', 'edge_assembled_contig_file', 'edge_assembler',
                  'edge_assembly_mink', 'edge_assembly_maxk', 'edge_assembly_step','edge_spades_algorithm',
                  'edge_spades_pacbio_file', 'edge_spades_nanopore_file', 'edge_megahit_preset', 'edge_assembly_minc',
                  'edge_r2c_aligner', 'edge_r2c_aligner_options', 'edge_r2c_getunmapped_sw' )


class AnnotationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Annotation
        fields = ('edge_anno_sw', 'edge_anno_size_cut', 'edge_anno_tool', 'edge_anno_kingdom', 'edge_anno_source_file')


class ReferenceGenomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReferenceGenome
        fields = ('name',)


class ReferenceGenomeFileSerializer(serializers.ModelSerializer):
    name = serializers.FilePathField(recursive=True, path=os.path.join(os.path.dirname(os.path.dirname(__file__)),
                                                                       'user_files'))
    class Meta:
        model = ReferenceGenomeFile
        fields = ('name', )


class ReferenceBasedAnalysisSerializer(serializers.ModelSerializer):
    reference_genomes = serializers.SlugRelatedField(many=True, queryset=ReferenceGenome.objects.all(), slug_field='name')
    edge_ref_file = ReferenceGenomeFileSerializer(many=True, required=False)
    class Meta:
        model = ReferenceBasedAnalysis
        fields = ('edge_ref_sw', 'edge_ref_file_fromlist', 'edge_ref_file',
                  'edge_r2g_aligner', 'edge_r2g_variantcall_sw', 'edge_r2g_getconsensus_sw', 'edge_r2g_aligner_options',
                  'edge_ref_unmapped_read_class', 'edge_r2g_getmapped_sw',
                  'edge_r2g_getunmapped_sw', 'edge_r2g_con_min_mapQ', 'edge_r2g_con_min_cov', 'edge_r2g_con_max_cov',
                  'edge_r2g_con_min_baseQ', 'edge_r2g_con_alt_prop')


class TaxonomyClassificationSerializer(serializers.ModelSerializer):
    classification_tools = fields.MultipleChoiceField(choices=TaxonomyClassification.CLASSIFICATION_TOOL_CHOICES)
    class Meta:
        model = TaxonomyClassification
        fields = ('edge_taxa_sw', 'edge_taxa_allreads', 'edge_taxa_enabled_tools', 'splitrim_minq',
                  'edge_taxa_custom_db_tool', 'edge_taxa_custom_db_file', 'edge_contig_taxa_sw')


class PhylogeneticAnalysisGenomeFileSerializer(serializers.ModelSerializer):
    name = serializers.FilePathField(recursive=True, path=os.path.join(os.path.dirname(os.path.dirname(__file__)),
                                                                       'user_files'))
    class Meta:
        model = PhyloReferenceGenomeFile
        fields = ('name',)

class SRAAccessionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SRAAccessions
        fields = ('number',)

class PhylogeneticAnalysisSerializer(serializers.ModelSerializer):
    edge_phylo_ref_list = serializers.SlugRelatedField(many=True, required=False, queryset=ReferenceGenome.objects.all(), slug_field='name')
    edge_phylo_ref_list_file = PhylogeneticAnalysisGenomeFileSerializer(many=True, required=False)
    sra_accessions = SRAAccessionsSerializer(many=True, required=False)
    class Meta:
        model = PhylogeneticAnalysis
        fields = ('edge_phylo_sw', 'edge_phylo_tree', 'edge_phylo_patho', 'edge_phylo_ref_select',
                  'edge_phylo_ref_list_file', 'edge_phylo_ref_select_ref', 'edge_phylo_sra_acc',
                  'edge_phylo_bootstrap_sw', 'edge_phylo_bootstrap_num')


class GeneFamilyAnalysisSerializer(serializers.ModelSerializer):
    class Meta:
        model = GeneFamilyAnalysis
        fields = ('edge_sg_sw', 'edge_reads_sg_sw', 'edge_orfs_sg_sw', 'edge_sg_stool', 'edge_sg_identity_options',
                  'edge_sg_length_options')



class PrimerValidationSerializer(serializers.ModelSerializer):
    primer_fasta_sequences = AdapterFastaSerializer(many=True)
    class Meta:
        model = PrimerValidation
        fields = ('edge_primer_sw', 'edge_primer_valid_sw', 'edge_primer_valid_file', 'edge_primer_valid_mm')


class PrimerDesignSerializer(serializers.ModelSerializer):
    class Meta:
        model = PrimerDesign
        fields = ('edge_primer_adj_sw', 'edge_primer_adj_tm_opt', 'edge_primer_adj_tm_min', 'edge_primer_adj_tm_max',
                  'edge_primer_adj_len_opt', 'edge_primer_adj_len_min', 'edge_primer_adj_len_max', 'edge_primer_adj_df',
                  'edge_primer_adj_num')


class JobSerializer(serializers.ModelSerializer):
    project = ProjectSerializer(required=True)
    sra_inputs = SRAInputSerializer(many=True, required=False)
    quality_trim_filter = QualityTrimFilterSerializer(required=False)
    stitch_paired_ends = StitchPairedEndsSerializer(required=False)
    host_removal = HostRemovalSerializer(required=False)
    read_fastq_input = ReadFastQInputSerializer(many=True, required=False)
    contig_fasta_input = ContigFastaInputSerializer(many=True, required=False)
    assembly = AssemblySerializer(required=False)
    annotation = AnnotationSerializer(required=False)
    reference_based_analysis = ReferenceBasedAnalysisSerializer(required=False)
    taxonomy_classification = TaxonomyClassificationSerializer(required=False)
    phylogenetic_analysis = PhylogeneticAnalysisSerializer(required=False)
    gene_family_analysis = GeneFamilyAnalysisSerializer(required=False)
    primer_validation = PrimerValidationSerializer(required=False)
    primer_design = PrimerDesignSerializer(required=False)

    class Meta:
        model = Job
        fields = ('project', 'start_time', 'sra_inputs', 'quality_trim_filter', 'stitch_paired_ends', 'host_removal',
                  'read_fastq_input', 'assembly', 'annotation', 'reference_based_analysis', 'taxonomy_classification',
                  'phylogenetic_analysis', 'gene_family_analysis', 'primer_validation',
                  'primer_design', 'contig_fasta_input')

    def create(self, validated_data):
        sra_data, read_fastq_input_data = [], []
        quality_trim_filter_data, assembly_data, annotation_data,stitch_paired_ends_data = None, None, None, None
        reference_analysis_data, taxonomy_classification_data = None, None
        phylogenetic_analysis_data, gene_family_analysis_data, primer_validation_data = None, None, None
        primer_design_data, host_removal_data, contig_fasta_input_data = None, None, None

        project_data = validated_data.pop('project')
        Project.objects.create(**project_data)
        current_project = Project.objects.get(name=project_data['name'])

        if 'read_fastq_input' in validated_data.keys():
            read_fastq_input_data = validated_data.pop('read_fastq_input')
        if 'sra_inputs' in validated_data.keys():
            sra_data = validated_data.pop('sra_inputs')
        if 'contig_fasta_input' in validated_data.keys():
            contig_fasta_input_data = validated_data.pop('contig_fasta_input')
        if 'quality_trim_filter' in validated_data.keys():
            quality_trim_filter_data = validated_data.pop('quality_trim_filter')
        if 'stitch_paired_ends' in validated_data.keys():
            stitch_paired_ends_data = validated_data.pop('stitch_paired_ends')
        if 'host_removal' in validated_data.keys():
            host_removal_data = validated_data.pop('host_removal')
        if 'assembly' in validated_data.keys():
            assembly_data = validated_data.pop('assembly')
        if 'annotation' in validated_data.keys():
            annotation_data = validated_data.pop('annotation')
        if 'reference_based_analysis' in validated_data.keys():
            reference_analysis_data = validated_data.pop('reference_based_analysis')
        if 'taxonomy_classification' in validated_data.keys():
            taxonomy_classification_data = validated_data.pop('taxonomy_classification')
        if 'phylogenetic_analysis' in validated_data.keys():
            phylogenetic_analysis_data = validated_data.pop('phylogenetic_analysis')
        if 'gene_family_analysis' in validated_data.keys():
            gene_family_analysis_data = validated_data.pop('gene_family_analysis')
        if 'primer_validation' in validated_data.keys():
            primer_validation_data = validated_data.pop('primer_validation')
        if 'primer_design' in validated_data.keys():
            primer_design_data = validated_data.pop('primer_design')
        job = Job.objects.create(project=current_project, **validated_data)

        # Pre-processing
        if len(read_fastq_input_data) > 0:
            # read_fastq_input_data = [bleach.clean(read_fastq_input_data[genome_file]['fastq_read_file']) for genome_file in
            #                          read_fastq_input_data]
            for read_fastq in read_fastq_input_data:
                ReadFastqInput.objects.create(job=job, **read_fastq)

        if len(sra_data) > 0:
            for sra in sra_data:
                sra['accession_number'] = bleach.clean(sra['edge_sra_acc'])
                SRAInput.objects.create(job=job, **sra)

        if contig_fasta_input_data:
            contig_fasta_input_data = {key:bleach.clean(str(val)) for (key, val) in contig_fasta_input_data.items()}
            ContigFastaInput.objects.create(job=job, **contig_fasta_input_data)

        if quality_trim_filter_data:
            quality_trim_filter_data = {key:bleach.clean(str(val)) for (key, val) in quality_trim_filter_data.items()}
            QualityTrimFilter.objects.create(job=job, **quality_trim_filter_data)

        if stitch_paired_ends_data:
            stitch_paired_ends_data = {key: bleach.clean(str(val)) for (key, val) in stitch_paired_ends_data.items()}
            StitchPairedEnds.objects.create(job=job, **stitch_paired_ends_data)

        if host_removal_data:
            host_removal_data = {key: bleach.clean(str(val)) for (key, val) in host_removal_data.items()}
            if 'edge_hostrm_file_fromlist' in self.initial_data['host_removal'].keys():
                host_genomes_data = self.initial_data['host_removal']['edge_hostrm_file_fromlist']
                host_genomes_data = [bleach.clean(genome) for genome in host_genomes_data]
                host_removal_data.pop('edge_hostrm_file_fromlist')
                HostRemoval.objects.create(job=job, **host_removal_data)
                host_removal = HostRemoval.objects.get(job=job)
                host_genomes_objs = HostGenome.objects.filter(name__in=host_genomes_data)
                for obj in host_genomes_objs:
                    host_removal.edge_hostrm_file_fromlist.add(obj)
            else:
                HostRemoval.objects.create(job=job, **host_removal_data)

        # Assembly and Annotation
        if assembly_data:
            assembly_data = {key: bleach.clean(str(val)) for (key, val) in assembly_data.items()}
            Assembly.objects.create(job=job, **assembly_data)
        if annotation_data:
            annotation_data = {key: bleach.clean(str(val)) for (key, val) in annotation_data.items()}
            Annotation.objects.create(job=job, **annotation_data)


        if reference_analysis_data:
            reference_analysis_data = {key: bleach.clean(str(val)) for (key, val) in reference_analysis_data.items()}
            reference_genomes_data, ref_genome_files_data = [], []
            if 'edge_ref_file_fromlist' in self.initial_data['reference_based_analysis'].keys():
                reference_genomes_data = self.initial_data['reference_based_analysis']['edge_ref_file_fromlist']
                reference_genomes_data = [bleach.clean(genome) for genome in reference_genomes_data]
                reference_analysis_data.pop('edge_ref_file_fromlist')

            if 'edge_ref_file' in self.initial_data['reference_based_analysis'].keys():
                ref_genome_files_data = self.initial_data['reference_based_analysis']['edge_ref_file']
                ref_genome_files_data = [bleach.clean(ref_genome_files_data[genome_file]) for genome_file in ref_genome_files_data]
                reference_analysis_data.pop('edge_ref_file')

            ReferenceBasedAnalysis.objects.create(job=job, **reference_analysis_data)

            if len(reference_genomes_data) + len(ref_genome_files_data) > 0:
                reference_analysis = ReferenceBasedAnalysis.objects.get(job=job)
                if len(reference_genomes_data) > 0:
                    reference_genome_objs = ReferenceGenome.objects.filter(name__in=reference_genomes_data)
                    for ref_obj in reference_genome_objs:
                        reference_analysis.reference_genomes.add(ref_obj)
                if len(ref_genome_files_data) > 0:
                    for ref_obj in ref_genome_files_data:
                        reference_analysis.reference_genome_files.create(name=ref_obj)


        if taxonomy_classification_data:
            taxonomy_classification_data = {key: bleach.clean(str(val)) for (key, val) in taxonomy_classification_data.items()}
            TaxonomyClassification.objects.create(job=job, **taxonomy_classification_data)

        if phylogenetic_analysis_data:
            phylogenetic_analysis_data = {key: bleach.clean(str(val)) for (key, val) in
                                          phylogenetic_analysis_data.items()}
            phylo_ref_genomes_data, phylo_ref_genome_files_data, sra_accessions_data = [], [], []
            if 'edge_phylo_ref_select' in self.initial_data['phylogenetic_analysis'].keys():
                phylo_ref_genomes_data = self.initial_data['phylogenetic_analysis']['edge_phylo_ref_select']
                phylo_ref_genomes_data = [bleach.clean(genome) for genome in phylo_ref_genomes_data]
                phylogenetic_analysis_data.pop('edge_phylo_ref_select')
            if 'edge_phylo_ref_list_file' in self.initial_data['phylogenetic_analysis'].keys():
                phylo_ref_genome_files_data = self.initial_data['phylogenetic_analysis']['edge_phylo_ref_list_file']
                phylo_ref_genome_files_data = [bleach.clean(genome_file['name']) for genome_file in
                                               phylo_ref_genome_files_data]
                phylogenetic_analysis_data.pop('edge_phylo_ref_list_file')
            if 'edge_phylo_sra_acc' in self.initial_data['phylogenetic_analysis'].keys():
                sra_accessions_data = self.initial_data['phylogenetic_analysis']['edge_phylo_sra_acc']
                # sra_accessions_data = [bleach.clean(genome['number']) for genome in
                #                        sra_accessions_data]
                for sra in sra_accessions_data:
                    sra['edge_phylo_sra_acc'] = bleach.clean(sra['edge_phylo_sra_acc'])
                phylogenetic_analysis_data.pop('edge_phylo_sra_acc')

            PhylogeneticAnalysis.objects.create(job=job, **phylogenetic_analysis_data)
            if len(phylo_ref_genomes_data) + len(phylo_ref_genome_files_data) + len(sra_accessions_data) > 0:
                phylogenetic_analysis = PhylogeneticAnalysis.objects.get(job=job)

                if len(phylo_ref_genomes_data) > 0:
                    phylo_ref_genome_objs = ReferenceGenome.objects.filter(name__in=phylo_ref_genomes_data)
                    for phylo_obj in phylo_ref_genome_objs:
                        phylogenetic_analysis.phylo_reference_genomes.add(phylo_obj)
                if len(phylo_ref_genome_files_data) > 0:
                    for ref_file in phylo_ref_genome_files_data:
                        phylogenetic_analysis.phylo_reference_genome_files.create(name=ref_file)
                if len(sra_accessions_data) > 0:
                    for sra in sra_accessions_data:
                        phylogenetic_analysis.sra_accessions.create(**sra)

        if gene_family_analysis_data:
            gene_family_analysis_data = {key: bleach.clean(str(val)) for (key, val) in gene_family_analysis_data.items()}
            GeneFamilyAnalysis.objects.create(job=job, **gene_family_analysis_data)

        if primer_validation_data:
            primer_validation_data = {key: bleach.clean(str(val)) for (key, val) in primer_validation_data.items()}
            PrimerValidation.objects.create(job=job, **primer_validation_data)

        if primer_design_data:
            primer_design_data = {key: bleach.clean(str(val)) for (key, val) in primer_design_data.items()}
            PrimerDesign.objects.create(job=job, **primer_design_data)

        return job

