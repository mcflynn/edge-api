"""Ctl_EDGE URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from rest_framework import routers
from controller import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'projects', views.ProjectViewSet)
router.register(r'metadata', views.MetadataViewSet)
router.register(r'jobs', views.JobViewSet)
router.register(r'contigfastafile', views.ContigFastaInputViewSet)
router.register(r'readfastq', views.ReadFastQInputViewSet)
router.register(r'srainputs', views.SRAInputViewSet)
router.register(r'qualitytrimfilters', views.QualityTrimFilterViewSet)
router.register(r'stitchpairedends', views.StitchPairedEndsViewSet)
router.register(r'hostgenomes', views.HostGenomeViewSet)
router.register(r'hostremovals', views.HostRemovalViewSet)
router.register(r'assemblies', views.AssemblyViewSet)
router.register(r'annotations', views.AnnotationViewSet)
router.register(r'taxonomy', views.TaxonomyClassificationViewSet)
router.register(r'reference', views.ReferenceBasedAnalysisViewSet)
router.register(r'phylogenetic', views.PhylogeneticAnalysisViewSet)
router.register(r'genefamily', views.GeneFamilyAnalysisViewSet)
router.register(r'primervalidation', views.PrimerValidationViewSet)
router.register(r'primerdesign', views.PrimerDesignViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^config/', include('controller.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]